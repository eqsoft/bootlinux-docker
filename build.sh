#!/bin/bash
. .env

docker build \
    --build-arg DEBIAN=$DEBIAN \
    -t $IMAGE_NAME .
