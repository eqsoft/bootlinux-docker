# base image for building bootlinux 
# see: https://gitlab.com/eqsoft/bootlinux

FROM library/debian:stretch-slim as linux

LABEL maintainer="schneider@hrz.uni-marburg.de"

ARG DEBIAN
ENV DEBIAN=$DEBIAN

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y --no-install-recommends \
    wget \
    gnupg \
    apt-transport-https \
    iproute2 \
    ca-certificates build-essential \
    liblzma-dev

RUN apt-get update && apt-get install -y --no-install-recommends \
    gettext-base \
    live-build \
    live-boot \
    live-config \
    squashfs-tools \
    syslinux \
    syslinux-common \
    nano \
    zip \
    unzip \
&& apt-get clean && apt-get autoremove

